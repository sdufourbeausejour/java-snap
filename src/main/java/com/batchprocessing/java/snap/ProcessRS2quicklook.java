/**
 *
 * @author dufobeso S. Dufour-Beauséjour PhD student at INRS-ETE Québec, Canada
 * s.dufour.beausejour@gmail.com
 *
 * RS2 quad-pol image processing steps:
 - Read RS2 product.xml
 - Create a subset (optional)
 - Apply radiometric calibration
 - Apply speckle filter
 - Compute polarimetric parameters ratios
 - Terrain correction (optional external DEM)
 - Get intersection of product with water mask (optional)
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import java.util.HashMap;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Product;

public class ProcessRS2quicklook {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    @SuppressWarnings("empty-statement")
    public static void main(File imageFile, File workingDir, File subsetFile, File DEMFile, File maskVectorFile) throws IOException, ParseException, java.text.ParseException {
        /* Input variables :
         imageFile (File)    image product.xml
         workingDir (File)   working directory
         subsetFile (File)   subset shapefile (optional)
         DEMFile (File)      external DEM TIFF file (optional)
         maskFile (File)     mask shapefile (optional) */


         String imageName = Utils.getImageName(imageFile);
         System.out.println("Shorter image name is: " + imageName);

         /* Create output directory (in workingDir) if it doesn't exist */
         File outputDirectory = Utils.getOutputDirectory(workingDir, imageFile);

         /* Create a readme.txt file if it doesn't exist */
         Utils.createReadme(outputDirectory);

         /* Convert RS2 product to Beam-DIMAP */
         String processingDone = new String();
         File dimFile = new File(outputDirectory, imageName + processingDone + ".dim");
         Product product = null;
         if (!(dimFile.exists())) {
             product = ReadProduct.RS2(imageFile);
             processingDone += "";
             WriteProduct.BEAMDIMAP(product, outputDirectory, dimFile);
             // product.dispose();
         }

         /* RGB quicklook : HH, HV, VV*/
         File quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
         if (!(quickFile.exists())) {
             product = ReadProduct.BEAMDIMAP(dimFile);
             System.out.println("Writing quicklook before subset : HH HV VV...");
             WriteProduct.quicklookRGB(product, quickFile, "Intensity_HH", "Intensity_HV", "Intensity_VV");
             // product.dispose();
         }
         //
         // /* Optionnal: Subset */
         // if (subsetFile != null) {
         //     File subFile = null;
         //     if (subsetFile.getName().contains("ROI")){
         //         subFile = new File(outputDirectory, imageName + processingDone + "_subROI.dim");
         //         processingDone += "_subROI";
         //     } else if (subsetFile.getName().contains("wide")){
         //         subFile = new File(outputDirectory, imageName + processingDone + "_subwide.dim");
         //         processingDone += "_subwide";
         //     }
         //
         //     Product subProduct = null;
         //     if (!(subFile.exists())) {
         //         product = ReadProduct.BEAMDIMAP(dimFile);
         //         subProduct = Subset.create(product, subsetFile);
         //         WriteProduct.BEAMDIMAP(subProduct, outputDirectory, subFile);
         //         subProduct.dispose();
         //     }
         //
         //     /* RGB quicklook : HH, HV, VV*/
         //     quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
         //     if (!(quickFile.exists())) {
         //         product = ReadProduct.BEAMDIMAP(subFile);
         //         System.out.println("Writing quicklook: HH HV VV...");
         //         WriteProduct.quicklookRGB(product, quickFile, "Intensity_HH", "Intensity_HV", "Intensity_VV");
         //         // product.dispose();
         //     }
         // }

          /* Radiometric calibration */
          File calFile = new File(outputDirectory, imageName + processingDone + "_cal.dim");
          Product calProduct = null;
          if (!(calFile.exists())) {
              product = ReadProduct.BEAMDIMAP(new File(outputDirectory, imageName + processingDone + ".dim"));
              calProduct = Calibrate.complex(product);
              WriteProduct.BEAMDIMAP(calProduct, outputDirectory, calFile);
              // product.dispose();
              calProduct.dispose();
          }
          processingDone += "_cal";

          /* Speckle filter: Lee Sigma 7x7 */
          File spkFile = new File(outputDirectory, imageName + processingDone + "_spk.dim");
          Product spkProduct = null;
          if (!(spkFile.exists())) {
              product = ReadProduct.BEAMDIMAP(calFile);
              spkProduct = SpeckleFilter.LeeSigma(product);
              WriteProduct.BEAMDIMAP(spkProduct, outputDirectory, spkFile);
              // product.dispose();
              spkProduct.dispose();
          }
          processingDone += "_spk";

        /* RGB quicklook : HH, HV, VV*/
        quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
        if (!(quickFile.exists())) {
            product = ReadProduct.BEAMDIMAP(spkFile);
            System.out.println("Writing quicklook : HH, HV, VV...");
            WriteProduct.quicklookRGB(product, quickFile, "Intensity_HH", "Intensity_HV", "Intensity_VV");
            // product.dispose();
        }
    }
}
