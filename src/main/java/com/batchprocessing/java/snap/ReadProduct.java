/*
 * Reading RS2 and BEAM-DIMAP products
 * - BEAMDIMAP : Reads and returns a BEAM-DIMAP .dim product (Product)
 * - BEAMDIMAPStack : Reads and returns a BEAM-DIMAP .dim product (Product)
 * - RS2 : Reads and returns a RS2 .xml product (Product)
 * - TSX : Reads and returns a TSX .xml product (Product)
 * - TIFF : Reads a .tif product, renames the bands according to bandNames, and returns the product (Product)
 * - TIFF : Reads and returns a .tif product (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.io.IOException;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class ReadProduct {

    /* Reading a BEAM-DIMAP product */
    public static Product BEAMDIMAP(File imageFile) throws IOException {
        //GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();
        System.out.println("Reading...");
        System.out.println(imageFile.getName());
        Product product = ProductIO.readProduct(imageFile);
        System.out.println("Done.");

        if (product == null) {
            System.out.println("Warning : Product is null.");
        }
        else {
            System.out.println("Product bands are :");
            for (String s : product.getBandNames()) {
                System.out.print(s+"  ");
            }
            System.out.print("\n");
        }
        return product;
    }

    /* Reading a BEAM-DIMAP product */
    public static Product BEAMDIMAPStack(String processingDone) throws IOException {

        File inFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\RADARSAT-2\\\\"+"Analyse\\\\ImageFiles\\\\" + processingDone + ".dim");

        System.out.println("Reading...");
        Product product = ProductIO.readProduct(inFile);
        System.out.println("Done.");

        if (product == null) {
            System.out.println("Warning : Product is null.");
        }
        else {
            System.out.println("Product bands are :");
            for (String s : product.getBandNames()) {
                System.out.print(s+"  ");
            }
            System.out.print("\n");
        }
        return product;
    }

    /* Reading a RS2 image product */
    public static Product RS2(File imageFile) throws IOException{
        System.out.println("Reading...");
        Product product = ProductIO.readProduct(imageFile);
        System.out.println("Done.");

        if (product == null) {
            System.out.println("Warning : Product is null.");
        }

        for (String s : product.getBandNames()) {
            System.out.println(s);
        }

        return product;
    }

    /* Reading a TSX image product */
    public static Product TSX(File imageFile) throws IOException{
        File productFile = Utils.getProductXML(imageFile);

        //File productFile = new File(imageFile, "product.xml");
        System.out.println("Reading...");
        Product product = ProductIO.readProduct(productFile);
        System.out.println("Done.");

        if (product == null) {
            System.out.println("Warning : Product is null.");
        }

        for (String s : product.getBandNames()) {
            System.out.println(s);
        }

        return product;
    }

    /* Reading a TIFF image product and renaming the bands  */
    public static Product TIFF(File imageFile, String[] bandNames) throws IOException{
        //File productFile = Utils.getProductXML(imageFile);

        File productFile = imageFile;
        System.out.println("Reading...");
        Product product = ProductIO.readProduct(productFile);
        for (int i = 0; i < bandNames.length; ++i){
            product.getBandAt(i).setName(bandNames[i]);
        }
        System.out.println("Done.");

        if (product == null) {
            System.out.println("Warning : Product is null.");
        }

        for (String s : product.getBandNames()) {
            System.out.println(s);
        }

        return product;
    }

/* Reading a TIFF image product   */
    public static Product TIFF(File imageFile) throws IOException{
        //File productFile = Utils.getProductXML(imageFile);

        File productFile = imageFile;
        System.out.println("Reading...");
        Product product = ProductIO.readProduct(productFile);
        System.out.println("Done.");

        if (product == null) {
            System.out.println("Warning : Product is null.");
        }

        for (String s : product.getBandNames()) {
            System.out.println(s);
        }
        return product;
    }
}
