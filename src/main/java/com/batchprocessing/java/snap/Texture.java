
/*
 * Extracting polarimetric parameters from the C3 matrix.
 * - ratios : create product containing the polarimetric ratios extracted from sigmaHH,HV,VH,VV
        HHVV, HHHV, VVVH ratios, Span (Product)
 * - HAalpha : create product from the H A alpha decomposition of the C3 matrix (Product)
 */

package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;
/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Texture {

    private static final double sqrt2 = Math.sqrt(2);
    private static final double pi = Math.PI;

    /* GLCM MEAN */
    public static Product GLCM_mean(Product product, String sourceBands) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("outputASM","false");
        parameters.put("outputContrast","false");
        parameters.put("outputCorrelation","false");
        parameters.put("outputDissimilarity","false");
        parameters.put("outputEnergy","false");
        parameters.put("outputEntropy","false");
        parameters.put("outputHomogeneity","false");
        parameters.put("outputMAX","false");
        parameters.put("outputVariance","false");

        parameters.put("outputMean","true");
        parameters.put("quantizationLevelsStr","32");
        parameters.put("quantizerStr","Probabilistic Quantizer");
        parameters.put("sourceBands",sourceBands);
        parameters.put("windowSizeStr","5x5");

        System.out.println("Computing GLCM texture MEAN for "+sourceBands);
        Product outProduct = GPF.createProduct("GLCM", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"GCLM texture MEAN from "+sourceBands+" (" + date + ").\n");
        outProduct.setName(product.getName()+"_tex");
        return outProduct;
    }

    /* GLCM MEAN (descriptive), VAR (descriptive), COR (descriptive), ASM (orderliness), HOM (contrast) */
    public static Product GLCM(Product product, String sourceBands) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("outputContrast","false");
        parameters.put("outputDissimilarity","false");
        parameters.put("outputEnergy","false");
        parameters.put("outputEntropy","false");
        parameters.put("outputMAX","false");

        parameters.put("outputMean","true");
        parameters.put("outputVariance","true");
        parameters.put("outputCorrelation","true");
        parameters.put("outputASM","true");
        parameters.put("outputHomogeneity","true");

        parameters.put("displacement","1");
        parameters.put("quantizationLevelsStr","32");
        parameters.put("quantizerStr","Probabilistic Quantizer");
        parameters.put("sourceBands",sourceBands);
        parameters.put("windowSizeStr","5x5");

        System.out.println("Computing GLCM texture MEAN for "+sourceBands);
        Product outProduct = GPF.createProduct("GLCM", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"GCLM texture MEAN from "+sourceBands+" (" + date + ").\n");
        outProduct.setName(product.getName()+"_tex2");
        return outProduct;
    }
}
