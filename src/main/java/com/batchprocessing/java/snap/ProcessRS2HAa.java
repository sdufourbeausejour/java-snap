/**
 *
 * @author dufobeso S. Dufour-Beauséjour PhD student at INRS-ETE Québec, Canada
 * s.dufour.beausejour@gmail.com
 *
 * RS2 quad-pol image processing steps:
 - Read RS2 product.xml
 - Create a subset (optional)
 - Apply radiometric calibration
 - Convert to C3 matrix
 - Apply polarimetric speckle filter
 - Compute polarimetric parameters HAalpha
 - Terrain correction (optional external DEM)
 - Get intersection of product with water mask (optional)
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import java.util.HashMap;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Product;

public class ProcessRS2HAa {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    @SuppressWarnings("empty-statement")
    public static void main(File imageFile, File workingDir, File subsetFile, File DEMFile, File maskVectorFile) throws IOException, ParseException, java.text.ParseException {
        /* Input variables :
         imageFile (File)    image product.xml
         workingDir (File)   working directory
         subsetFile (File)   subset shapefile (optional)
         DEMFile (File)      external DEM TIFF file (optional)
         maskFile (File)     mask shapefile (optional) */

        String imageName = Utils.getImageName(imageFile);
        System.out.println("Shorter image name is: " + imageName);

        /* Create output directory (in workingDir) if it doesn't exist */
        File outputDirectory = Utils.getOutputDirectory(workingDir, imageFile);

        /* Create a readme.txt file if it doesn't exist */
        Utils.createReadme(outputDirectory);

        /* Convert RS2 product to Beam-DIMAP */
        String processingDone = new String();
        File dimFile = new File(outputDirectory, imageName + processingDone + ".dim");
        Product product = null;
        if (!(dimFile.exists())) {
            product = ReadProduct.RS2(imageFile);
            processingDone += "";
            WriteProduct.BEAMDIMAP(product, outputDirectory, dimFile);
        }

        /* RGB quicklook : HH, HV, VV*/
        File quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
        if (!(quickFile.exists())) {
            product = ReadProduct.BEAMDIMAP(dimFile);
            System.out.println("Writing quicklook before subset : HH HV VV...");
            WriteProduct.quicklookRGB(product, quickFile, "Intensity_HH", "Intensity_HV", "Intensity_VV");
        }

        /* Optionnal: Subset */
        if (subsetFile != null) {
            File subFile = null;
            subFile = new File(outputDirectory, imageName + processingDone + "_sub.dim");

            Product subProduct = null;
            if (!(subFile.exists())) {
                product = ReadProduct.BEAMDIMAP(dimFile);
                subProduct = Subset.create(product, subsetFile);
                WriteProduct.BEAMDIMAP(subProduct, outputDirectory, subFile);
            }
            processingDone += "_sub";

            /* RGB quicklook : HH, HV, VV*/
            quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
            if (!(quickFile.exists())) {
                // product = ReadProduct.BEAMDIMAP(dimFile);
                System.out.println("Writing quicklook: HH HV VV...");
                WriteProduct.quicklookRGB(subProduct, quickFile, "Intensity_HH", "Intensity_HV", "Intensity_VV");
            }
        }

         /* Radiometric calibration */
         File calFile = new File(outputDirectory, imageName + processingDone + "_cal.dim");
         Product calProduct = null;
         if (!(calFile.exists())) {
             product = ReadProduct.BEAMDIMAP(new File(outputDirectory, imageName + processingDone + ".dim"));
             calProduct = Calibrate.complex(product);
             WriteProduct.BEAMDIMAP(calProduct, outputDirectory, calFile);
             calProduct.dispose();
         }
         processingDone += "_cal";

         /* Conversion to C3 matrix */
         File matFile = new File(outputDirectory, imageName + processingDone + "_C3.dim");
         Product matProduct = null;
         if (!(matFile.exists())) {
             product = ReadProduct.BEAMDIMAP(calFile);
             matProduct = Matrix.C3(product);
             WriteProduct.BEAMDIMAP(matProduct, outputDirectory, matFile);
         }
         processingDone += "_C3";

         /* Polarimetric speckle filter: Refined Lee 7x7 */
         File spkFile = new File(outputDirectory, imageName + processingDone + "_spk.dim");
         Product spkProduct = null;
         if (!(spkFile.exists())) {
             product = ReadProduct.BEAMDIMAP(matFile);
             spkProduct = SpeckleFilter.PolarimetricRefinedLee(product);
             WriteProduct.BEAMDIMAP(spkProduct, outputDirectory, spkFile);
             spkProduct.dispose();
         }
         processingDone += "_spk";

        /* RGB quicklook : C11, C22, C33*/
        quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
        if (!(quickFile.exists())) {
            product = ReadProduct.BEAMDIMAP(spkFile);
            System.out.println("Writing quicklook : C11, C22, C33...");
            WriteProduct.quicklookRGB(product, quickFile, "C11", "C22", "C33");
        }

        /* HAalpha Decomposition */
        File HAaFile = new File(outputDirectory, imageName + processingDone + "_HAa.dim");
        Product HAaProduct = null;
        if (!(HAaFile.exists())) {
            product = ReadProduct.BEAMDIMAP(spkFile);
            HAaProduct = Polarimetric.HAalpha(product);
            // Product combinedProduct = Utils.combine(product, HAaProduct);
            WriteProduct.BEAMDIMAP(HAaProduct, outputDirectory, HAaFile);
            HAaProduct.dispose();
        }
        processingDone += "_HAa";

        /* Terrain correction; if no external DEM, SRTM 3sec is used */
        File TCFile = new File(outputDirectory, imageName + processingDone + "_TC2.dim");
        Product TCProduct = null;
        if (!(TCFile.exists())) {
            product = ReadProduct.BEAMDIMAP(HAaFile);
            if (DEMFile != null){
                TCProduct = TerrainCorrection.RDTC(product, DEMFile);
            } else {
                TCProduct = TerrainCorrection.RDTC(product);
            }
            WriteProduct.BEAMDIMAP(TCProduct, outputDirectory, TCFile);
            TCProduct.dispose();
        }
        processingDone += "_TC2";

        /* GeoTIFF */
        File tiffFile = new File(outputDirectory, imageName + processingDone + ".tif");
        //System.out.println("Warning : deleting terrain corrected Tiff product and rewriting it");
        //tiffFile.delete();
        if (!(tiffFile.exists())) {
            product = ReadProduct.BEAMDIMAP(TCFile);
            WriteProduct.geoTIFF(product, tiffFile);
        }

        /* RGB quicklook : H, A, alpha*/
        quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
        if (!(quickFile.exists())) {
            product = ReadProduct.BEAMDIMAP(TCFile);
            System.out.println("Writing quicklook : H, A, Alpha...");
            WriteProduct.quicklookRGB(product, quickFile, "Entropy", "Anisotropy", "Alpha");
        }


        // /* Optionnal: Mask */
        // if (maskVectorFile != null) {
        //     // Writing of this product to Beam DIMAP is done in Masking.getIntersection
        //     File mskFile = new File(outputDirectory, imageName + processingDone + "_msk.dim");
        //     Product mskProduct = null;
        //     String maskVectorName = FilenameUtils.getBaseName(maskVectorFile.getName());
        //     if (!(mskFile.exists())) {
        //         TCProduct = ReadProduct.BEAMDIMAP(TCFile);
        //         TCProduct = Masking.addMask(TCProduct, maskVectorFile);
        //         mskProduct = Masking.getIntersection(TCProduct, maskVectorName, mskFile);
        //     }
        //     processingDone += "_msk";
        //
        //     /*Write to GeoTIFF */
        //     tiffFile = new File(outputDirectory, imageName + processingDone + ".tif");
        //     if (!(tiffFile.exists())) {
        //         mskProduct = ReadProduct.BEAMDIMAP(mskFile);
        //         WriteProduct.geoTIFF(mskProduct, tiffFile);
        //     }
        //
        //     /* RGB quicklook : H, A, alpha*/
        //     quickFile = new File(outputDirectory, imageName + processingDone + ".jpg");
        //     if (!(quickFile.exists())) {
        //         product = ReadProduct.BEAMDIMAP(mskFile);
        //         System.out.println("Writing quicklook : H, A, Alpha...");
        //         WriteProduct.quicklookRGB(product, quickFile, "Entropy", "Anisotropy", "Alpha");
        //     }
        // }
    }
}
