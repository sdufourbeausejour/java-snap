/*
 * Geometric terrain correction
 * - RDTC (product): Create product from Range-Doppler Terrain-Correction using default DEM (Product)
 * - RDTC (product, File): Create product from Range-Doppler Terrain-Correction using external DEM (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.util.SystemUtils;
import org.openide.util.Lookup;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class TerrainCorrection {

    /* With default DEM */
    public static Product RDTC(Product product){
        SystemUtils.initGeoTools();
        MySystemUtils.initJAI(Lookup.getDefault().lookup(ClassLoader.class));
        //SystemUtils.initJAI(ClassLoader.getSystemClassLoader());
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Create bandName list in string for parameter input */
        String[] sourceBandNames = product.getBandNames();
        String sourceBandNamesParameter = new String();
        int k = 1;
        for (String sourceBandName : sourceBandNames){
            sourceBandNamesParameter += sourceBandName;
            if (k<sourceBandNames.length){
                sourceBandNamesParameter += ",";
            }
            k++;
        }
        System.out.println(sourceBandNamesParameter);

        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("nodataValueAtSea","false");
        // parameters.put("outputComplex","false");
        // parameters.put("pixelSpacingInMeter","8");
        // parameters.put("saveLatLon","true");
        // parameters.put("saveLocalIncidenceAngle","true");
        // parameters.put("saveIncidenceAngleFromEllipsoid","true");
        // parameters.put("saveProjectedLocalIncidenceAngle","true");
        parameters.put("sourceBands",sourceBandNamesParameter);

        System.out.println("Terrain correction with default DEM (SRTM 3sec)...");
        Product outProduct = GPF.createProduct("Terrain-Correction", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Terrain correction (" + date + ").\n");
        outProduct.setName(product.getName()+"_TC");
        return outProduct;
    }

    /* With external DEM */
    public static Product RDTC(Product product, File DEMFile){
        SystemUtils.initGeoTools();
        MySystemUtils.initJAI(Lookup.getDefault().lookup(ClassLoader.class));
        //SystemUtils.initJAI(ClassLoader.getSystemClassLoader());
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Create bandName list in string for parameter input */
        String[] sourceBandNames = product.getBandNames();
        String sourceBandNamesParameter = new String();
        int k = 1;
        for (String sourceBandName : sourceBandNames){
            sourceBandNamesParameter += sourceBandName;
            if (k<sourceBandNames.length){
                sourceBandNamesParameter += ",";
            }
            k++;
        }
        System.out.println(sourceBandNamesParameter);

        /* Operator parameters*/
        HashMap parameters = new HashMap();

        parameters.put("nodataValueAtSea","false");
        /* If no external DEM, default is SRTM 3sec*/
        parameters.put("demName","External DEM");
        parameters.put("externalDEMFile", DEMFile);
        //parameters.put("mapProjection", "WGS84(DD)");
        parameters.put("mapProjection", "EPSG:32618");
        parameters.put("demResamplingMethod", "NEAREST_NEIGHBOUR");
        parameters.put("imageResamplingMethod", "NEAREST_NEIGHBOUR");
        // parameters.put("outputComplex","false");
        // parameters.put("pixelSpacingInMeter","8");
        // parameters.put("saveLatLon","true");
        // parameters.put("saveLocalIncidenceAngle","true");
        // parameters.put("saveIncidenceAngleFromEllipsoid","true");
        // parameters.put("saveProjectedLocalIncidenceAngle","true");
        parameters.put("sourceBands",sourceBandNamesParameter);

        System.out.println("Terrain correction with external DEM...");
        Product outProduct = GPF.createProduct("Terrain-Correction", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Terrain correction (" + date + ").\n");
        outProduct.setName(product.getName()+"_TC");
        return outProduct;
    }
}
