/*
 * Get the intersection of the product and a water mask
 * also multiply by a polygon created to crop the water area to the region of interest
 * - addMask: import mask vector into product (used to be addWater) (Product)
 * - getIntersection: create mask from water vector,
 *      create and write the intersection of the product with this mask (Product)
 * - getIntersection2: create masks from ROI and water vectors,
 *      create and write the intersection of the product with these masks (Product)
 * - setValueToNaN: set a numeric value to NaN instead (Product)
 */
package com.batchprocessing.java.snap;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import org.esa.s1tbx.io.PolBandUtils;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.dataio.ProductWriter;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.datamodel.ProductNode;
import org.esa.snap.core.datamodel.ProductNodeGroup;
import org.esa.snap.core.datamodel.VectorDataNode;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.datamodel.Mask;
import org.esa.snap.core.datamodel.ProductData;
import org.esa.snap.core.datamodel.RasterDataNode;
import org.esa.snap.core.gpf.common.BandMathsOp;
import org.esa.snap.core.util.ProductUtils;
import org.esa.snap.engine_utilities.gpf.OperatorUtils;

/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Masking {

    /* Add the mask raster to the product as a vector */
    public static Product addMask(Product product, File maskVectorFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        HashMap parameters = new HashMap();
        parameters.put("vectorFile", maskVectorFile);
        parameters.put("seperateShapes","True");

        // Imports vector shapefile into product
        Product outProduct = GPF.createProduct("Import-Vector", parameters, product);
        return outProduct;
    }

    /* Get the intersection of the mask and the product by creating a new product and manually adding the bands */
    public static Product getIntersection(Product product, String maskName, File outFile) throws IOException{
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        // Get array of bandNames before adding the mask band
        String[] bandNames = product.getBandNames();

        // Fetch vector to use as mask (vector has already been imported into product)
        ProductNodeGroup vectorDataGroup = product.getVectorDataGroup();
        //ProductNode waterVector = vectorDataGroup.get("Water_HudsonBay_1");
        for (String s : vectorDataGroup.getNodeNames()){
            // Find matching maskName
            if (s.contains(maskName)){
                maskName = s;
            }
        }
        ProductNode maskVector = vectorDataGroup.get(maskName);
        System.out.println(maskVector.getName());

        // Add mask to product from vector
        Mask mask = product.addMask(maskVector.getDisplayName(), (VectorDataNode) maskVector, maskName, Color.blue, 0);

        ////////// New product
        // Initialize new product
        System.out.println("Creating new product for intersection of mask and bands...");
        System.out.println("Product name : " + product.getName()+"_msk");
        System.out.println("Product type : " + product.getProductType());
        System.out.println("Scene raster width : " + product.getSceneRasterWidth()+"; mask :" +mask.getRasterWidth());
        System.out.println("Scene raster height : " + product.getSceneRasterHeight()+"; mask :"+mask.getRasterHeight());

        Product outProduct = new Product(product.getName()+"_msk", product.getProductType(), product.getSceneRasterWidth(), product.getSceneRasterHeight());
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Product multiplied by mask : "+maskName+" (" + date + ").\n");
        //ProductUtils.copyGeoCoding(product, outProduct);
        //ProductUtils.copyMetadata(product, outProduct);
        ProductUtils.copyProductNodes(product, outProduct);

        // Set product writer and write header
        ProductWriter writer = ProductIO.getProductWriter("BEAM-DIMAP");
        outProduct.setProductWriter(writer);

        // Initialize all bands so that they are written to the header, then write the header
        Band[] targetBands = new Band[bandNames.length];
        int k = 0;
        for (String bandName : bandNames) {
            targetBands[k] = outProduct.addBand(bandName, ProductData.TYPE_FLOAT32);
            targetBands[k].setNoDataValueUsed(true);
            k++;
        }

        outProduct.writeHeader(outFile);

        // Initialize data buffer which will be used for each scan line of band to be written; create divideBy float for normalization
        float[] maskDataBuffer = new float[mask.getRasterWidth()];
        float divideBy = (float) 255;

        /* Writing the bands
        Scan each line of the mask to data buffer, then scan each element and normalize to 0 or 1,
        multiply the mask element with the source band element and then write each line to the target band.
        java arrays = HxW and getPixels operator is HxW */
        k = 0;
        float[] bandDataBuffer = new float[mask.getRasterWidth()];
        for (String bandName : bandNames) {
            Band sourceBand = product.getBand(bandName);
            Band targetBand = targetBands[k];

            for (int i = 0; i < mask.getRasterHeight(); i++) {
                maskDataBuffer = (mask.readPixels(0, i, mask.getRasterWidth(), 1, maskDataBuffer)); // values are 0.0 or 255.0
                bandDataBuffer = (sourceBand.readPixels(0, i, sourceBand.getRasterWidth(), 1, bandDataBuffer));

                for (int j = 0; j < mask.getRasterWidth(); j++) {
                    maskDataBuffer[j] = maskDataBuffer[j] / divideBy;
                    bandDataBuffer[j] = bandDataBuffer[j]*maskDataBuffer[j];
                }


                targetBand.writePixels(0, i, mask.getRasterWidth(), 1, bandDataBuffer);
            }
            k++;
            //System.out.println(Arrays.toString(bandDataBuffer));
            //System.out.println(Arrays.toString(maskDataBuffer));
        }
        product.closeIO();
        System.out.println("Done.");

        System.out.println("Product bands are :");
        for (String s : outProduct.getBandNames()) {
            System.out.println(s);
        }
        return outProduct;
    }

    /* Hard-coded : Get the intersection of two masks and the product by creating a new product and manually adding the bands */
    public static Product getIntersection2(Product product, File imageFile, String processingDone, String maskName1, String maskName2, File outFile) throws IOException{
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        // Get array of bandNames before adding the mask band
        String[] bandNames = product.getBandNames();
        System.out.println("Product raster data nodes are :");
        for (RasterDataNode s : product.getRasterDataNodes()) {
            System.out.println(s);
        }
        // Fetch vectors to use as masks (vectors have already been imported into product)
        ProductNodeGroup vectorDataGroup = product.getVectorDataGroup();
        ProductNode maskVector1 = vectorDataGroup.get(maskName1);
        ProductNode maskVector2 = vectorDataGroup.get(maskName2);
        System.out.println("First mask : "+maskVector1.getName());
        System.out.println("Second mask : "+maskVector2.getName());
        // Add masks to product from vectors
        Mask mask1 = product.addMask(maskVector1.getDisplayName(), (VectorDataNode) maskVector1, maskName1, Color.blue, 0);
        Mask mask2 = product.addMask(maskVector2.getDisplayName(), (VectorDataNode) maskVector2, maskName2, Color.green, 0);

        /* New product */
        // Initialize new product
        System.out.println("Creating new product for intersection of masks and bands...");
        System.out.println("Product name : " + product.getName()+"_msk");
        System.out.println("Product type : " + product.getProductType());
        System.out.println("Scene raster width : " + product.getSceneRasterWidth()+"; mask :" +mask1.getRasterWidth());
        System.out.println("Scene raster height : " + product.getSceneRasterHeight()+"; mask :"+mask1.getRasterHeight());

        Product outProduct = new Product(product.getName()+"_msk", product.getProductType(), product.getSceneRasterWidth(), product.getSceneRasterHeight());
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Product multiplied by 2 masks : "+maskName1+" "+maskName2+" (" + date + ").\n");
        //ProductUtils.copyGeoCoding(product, outProduct);
        //ProductUtils.copyMetadata(product, outProduct);
        ProductUtils.copyProductNodes(product, outProduct);

        // Set product writer and write header
        ProductWriter writer = ProductIO.getProductWriter("BEAM-DIMAP");
        outProduct.setProductWriter(writer);

        // Utils
        File workingDirectory = Utils.createWorkingDirectory(imageFile);
        String imageName = Utils.getImageName(imageFile);

        // Initialize all bands so that they are written to the header, then write the header
        Band[] targetBands = new Band[bandNames.length];
        int k = 0;
        for (String bandName : bandNames) {
            targetBands[k] = outProduct.addBand(bandName, ProductData.TYPE_FLOAT32);
            targetBands[k].setNoDataValueUsed(true);
            k++;
        }
        outProduct.writeHeader(outFile);

        // Initialize data buffer which will be used for each scan line of band to be written; create divideBy float for normalization
        float[] mask1DataBuffer = new float[mask1.getRasterWidth()];
        float[] mask2DataBuffer = new float[mask2.getRasterWidth()];
        float divideBy = (float) 255;

        /* Writing the bands
        Scan each line of the masks to data buffer, then scan each element and normalize to 0 or 1,
        multiply the mask elements with the source band element and then write each line to the target band.
        java arrays = HxW and getPixels operator is HxW */
        k = 0;
        float[] bandDataBuffer = new float[mask1.getRasterWidth()];
        for (String bandName : bandNames) {
            Band sourceBand = product.getBand(bandName);
            Band targetBand = targetBands[k];

            for (int i = 0; i < mask1.getRasterHeight(); i++) {
                mask1DataBuffer = (mask1.readPixels(0, i, mask1.getRasterWidth(), 1, mask1DataBuffer)); // values are 0.0 or 255.0
                mask2DataBuffer = (mask2.readPixels(0, i, mask2.getRasterWidth(), 1, mask2DataBuffer)); // values are 0.0 or 255.0
                bandDataBuffer = (sourceBand.readPixels(0, i, sourceBand.getRasterWidth(), 1, bandDataBuffer));

                for (int j = 0; j < mask1.getRasterWidth(); j++) {
                    mask1DataBuffer[j] = mask1DataBuffer[j] / divideBy;
                    mask2DataBuffer[j] = mask2DataBuffer[j] / divideBy;
                    bandDataBuffer[j] = bandDataBuffer[j]*mask1DataBuffer[j]*mask2DataBuffer[j];
                }
                targetBand.writePixels(0, i, mask1.getRasterWidth(), 1, bandDataBuffer);
            }
            k++;
            //System.out.println(Arrays.toString(bandDataBuffer));
            //System.out.println(Arrays.toString(mask1DataBuffer));
        }
        product.closeIO();
        System.out.println("Done.");

        /* Update readme.txt */
        File readmeFile = Utils.updateReadme(workingDirectory);
        //String date = Utils.getTimeStamp();

        try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
            readme.append("Wrote product " + outFile.getName() + "  (" + date + ")\n");
        }

        System.out.println("Product bands are :");
        for (String s : outProduct.getBandNames()) {
            System.out.println(s);
        }

        return outProduct;
    }

    /* Set a numeric value to NaN, ex. 0 to NaN */
    public static Product setValueToNaN(Product sourceProduct, int noDataValue){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        String[] sourceBandNames = sourceProduct.getBandNames();
        BandMathsOp.BandDescriptor[] targetBands = new BandMathsOp.BandDescriptor[sourceBandNames.length];
        int i = 0;
        // New bands
        for(String name : sourceBandNames){
            System.out.println("Adding targetBand "+name);
            sourceProduct.getBand(name).setName(name+"_old");
            targetBands[i] = new BandMathsOp.BandDescriptor();
            targetBands[i].name = name;
            targetBands[i].expression = "if "+name+"_old == "+Integer.toString(noDataValue)+" then NaN else "+name+"_old";
            i +=1;
        }
        for (BandMathsOp.BandDescriptor targetBand : targetBands){
            System.out.println("Setting type and noDataValue"+targetBand.name);
            targetBand.type = "float32";
            targetBand.noDataValue = sourceProduct.getBandAt(0).getNoDataValue();
        }
        HashMap parameters = new HashMap();
        parameters.put("targetBands",targetBands);

        System.out.println("Setting "+Integer.toString(noDataValue));
        Product outProduct = GPF.createProduct("BandMaths", parameters, sourceProduct);
        System.out.println("Done.");
        return outProduct;
    }
}
