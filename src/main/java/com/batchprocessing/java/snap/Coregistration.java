/*
 * Coregistrate products 
 * - createStack : stack products from array (Product)
 * - crossCorrelation : find GCPs (Product)
 * - computeWarp : compute warp function between master and slave(s) (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;
/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Coregistration {
    
    /* Create Stack */
    public static Product createStack(Product[] products, File[] imageFiles) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        HashMap parameters = new HashMap();
        HashMap productMap = new HashMap();
        parameters.put("extent","Master");
        parameters.put("initialOffsetMethod","Orbit");        
        parameters.put("resamplingType","NONE");
        
        productMap.put("master",products[0]);
        Product[] slaveProducts = Arrays.copyOfRange(products, 1, products.length);
        productMap.put("slave",slaveProducts);
              
        
        System.out.println("Creating stack...");
        Product outProduct = GPF.createProduct("CreateStack", parameters, products);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription("Create stack from : \n"+ Utils.getImageStackName(imageFiles) +" (" + date + ").\n");
        outProduct.setName(Utils.getCollapsedImageStackName(imageFiles));
                
        return outProduct;           
    }
    
    /* Cross-correlation */
    public static Product crossCorrelation(Product product, HashMap parameters){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        /* These are default values. If not enough GCPs survive, increase numGCP 
        to 1000. If the offset between master and slave images is large, 
        increase coarse registration window dimensions to 256, or 512. From S1TBX InSAR tutorial.*/        
        parameters.put("applyFineRegistration","true");
        parameters.put("inSAROptimized","true");        
        parameters.put("fineRegistrationWindowWidth","32");        
        parameters.put("fineRegistrationWindowHeight","32");        
        parameters.put("coherenceThreshold","0.6");        
        parameters.put("fineRegistrationWindowAccAzimuth","16");        
        parameters.put("fineRegistrationWindowAccRange","16");        
        parameters.put("fineRegistrationOversampling","16");      
        parameters.put("useSlidingWindow","false");
        
              
        System.out.println("Cross-correlating...");
        Product outProduct = GPF.createProduct("Cross-Correlation", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Used cross-correlation to find optimal GCPs in stack. numGCPs = "+ parameters.get("numGCPtoGenerate") +" (" + date + ").\n");

        return outProduct;
    }
    
    /* Compute warp function */
    public static Product computeWarp(Product product, HashMap parameters){
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        /* Default values are rms = 0.05, warpPolynomial = 2. The lower the rms 
        threshold can be while having enough surviving GCPs, the better the 
        coregistration. Higher order polynomials should only be used for images 
        that have been greatly distorted. (S1TBX InSAR Tutorial)*/
        parameters.put("interpolationMethod","Cubic convolution (6 points)");    
         
        System.out.println("Computing warp function...");
        Product outProduct = GPF.createProduct("Warp", parameters, product);        
        System.out.println("Done.");
        
        /* Update product description and name */
        String date = Utils.getTimeStamp();
        
        outProduct.setDescription(product.getDescription()+"Computed warp function for coregistration (" + date + ").\n");

        return outProduct;
    }    
    
}
