/**
 *
 * @author dufobeso S. Dufour-Beauséjour PhD student at INRS-ETE Québec, Canada
 * s.dufour.beausejour@gmail.com
 *
 * RS2 quad-pol image processing steps:
 - Read processed files
 - Compute GLCM texture parameter MEAN
 - Terrain correction with external DEM
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import java.util.HashMap;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Product;

public class ProcessRS2texture {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    @SuppressWarnings("empty-statement")
    public static void postTC(File imageFile, File workingDir) throws IOException, ParseException, java.text.ParseException {
        /* Input variables :
         imageFile (File)    image product.xml
         workingDir (File)   working directory*/

        String imageName = Utils.getImageName(imageFile);
        System.out.println("Shorter image name is: " + imageName);

        /* Create output directory (in workingDir) if it doesn't exist */
        File outputDirectory = Utils.getOutputDirectory(workingDir, imageFile);

        /* Create a readme.txt file if it doesn't exist */
        Utils.createReadme(outputDirectory);
        Product product = null;

        /* Read processed file */
        String processingDone = new String("");
        File HAaFile = new File(outputDirectory, imageName + "_sub_cal_C3_spk_HAa_TC2.tif");
        File ratFile = new File(outputDirectory, imageName + "_sub_cal_spk_rat2_TC2.tif");

        File texFile = new File(outputDirectory, imageName + processingDone + "_tex2.dim");
        Product texProduct = null;
        if (!(texFile.exists())) {
            Product HAaProduct = ReadProduct.TIFF(HAaFile);
            Product ratProduct = ReadProduct.TIFF(ratFile);
            Product combinedProduct = Utils.combine(HAaProduct, ratProduct);
            String sourceBands = new String("Intensity_HH,Intensity_HV,Intensity_VV");
            // texProduct = Texture.GLCM_mean(combinedProduct, sourceBands);
            texProduct = Texture.GLCM(combinedProduct, sourceBands);
            WriteProduct.BEAMDIMAP(texProduct, outputDirectory, texFile);
            HAaProduct.dispose();
            ratProduct.dispose();
        }
        processingDone += "_tex2";

        /* GeoTIFF */
        File tiffFile = new File(outputDirectory, imageName + processingDone + ".tif");
        //System.out.println("Warning : deleting terrain corrected Tiff product and rewriting it");
        //tiffFile.delete();
        if (!(tiffFile.exists())) {
            product = ReadProduct.BEAMDIMAP(texFile);
            WriteProduct.geoTIFF(product, tiffFile);
        }
    }
    public static void preTC(File imageFile, File workingDir, File DEMFile) throws IOException, ParseException, java.text.ParseException {
        /* Input variables :
         imageFile (File)    image product.xml
         workingDir (File)   working directory
         DEMFile (File)      external DEM TIFF file (optional)*/

        String imageName = Utils.getImageName(imageFile);
        System.out.println("Shorter image name is: " + imageName);

        /* Create output directory (in workingDir) if it doesn't exist */
        File outputDirectory = Utils.getOutputDirectory(workingDir, imageFile);

        /* Create a readme.txt file if it doesn't exist */
        Utils.createReadme(outputDirectory);
        Product product = null;

        /* Read processed file */
        String processingDone = new String("");
        File HAaFile = new File(outputDirectory, imageName + "_sub_cal_C3_spk_HAa.dim");
        File ratFile = new File(outputDirectory, imageName + "_sub_cal_spk_rat2.dim");

        File texFile = new File(outputDirectory, imageName + processingDone + "_tex2.dim");
        Product texProduct = null;
        if (!(texFile.exists())) {
            Product HAaProduct = ReadProduct.TIFF(HAaFile);
            Product ratProduct = ReadProduct.TIFF(ratFile);
            Product combinedProduct = Utils.combine(HAaProduct, ratProduct);
            String sourceBands = new String("Intensity_HH,Intensity_HV,Intensity_VV");
            // texProduct = Texture.GLCM_mean(combinedProduct, sourceBands);
            texProduct = Texture.GLCM(combinedProduct, sourceBands);
            WriteProduct.BEAMDIMAP(texProduct, outputDirectory, texFile);
            HAaProduct.dispose();
            ratProduct.dispose();
        }
        processingDone += "_tex2";

        /* Terrain correction; if no external DEM, SRTM 3sec is used */
         File TCFile = new File(outputDirectory, imageName + processingDone + "_TC2.dim");
         Product TCProduct = null;
         if (!(TCFile.exists())) {
             product = ReadProduct.BEAMDIMAP(texFile);
             if (DEMFile != null){
                 TCProduct = TerrainCorrection.RDTC(product, DEMFile);
             } else {
                 TCProduct = TerrainCorrection.RDTC(product);
             }
             WriteProduct.BEAMDIMAP(TCProduct, outputDirectory, TCFile);
             TCProduct.dispose();
         }
         processingDone += "_TC2";

        /* GeoTIFF */
        File tiffFile = new File(outputDirectory, imageName + processingDone + ".tif");
        //System.out.println("Warning : deleting terrain corrected Tiff product and rewriting it");
        //tiffFile.delete();
        if (!(tiffFile.exists())) {
            product = ReadProduct.BEAMDIMAP(TCFile);
            WriteProduct.geoTIFF(product, tiffFile);
        }
    }
}
