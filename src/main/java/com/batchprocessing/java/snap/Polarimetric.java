
/*
 * Extracting polarimetric parameters from the C3 matrix.
 * - ratios : create product containing the polarimetric ratios extracted from sigmaHH,HV,VH,VV
        HHVV, HHHV, VVVH ratios, Span (Product)
 * - HAalpha : create product from the H A alpha decomposition of the C3 matrix (Product)
 */

package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.gpf.common.BandMathsOp;
/**
 *
 * @author dufobeso
 * S. Dufour-Beauséjour
 * PhD student at INRS-ETE
 * Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Polarimetric {

    private static final double sqrt2 = Math.sqrt(2);
    private static final double pi = Math.PI;

    /* Get polarimetric ratios (HHVV, HHHV, VVVH ratios - needs HH, VV etc as input */
    public static Product ratios(Product product) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("useMeanMatrix","true");
        parameters.put("windowSizeXStr","5");
        parameters.put("windowSizeXStr","5");
        parameters.put("outputSpan","false");
        parameters.put("outputHHVVRatio","true");
        parameters.put("outputHHHVRatio","true");
        parameters.put("outputVVVHRatio","true");

        System.out.println("Computing polarimetric ratios from HH,HV,VH,VV...");
        Product outProduct = GPF.createProduct("Polarimetric-Parameters", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Polarimetric ratios HHVV HHHV VVVH (" + date + ").\n");
        outProduct.setName(product.getName()+"_polparam");
        return outProduct;
    }

    /* Get polarimetric ratios (VVHH, HVHH, VHVV ratios */
    public static Product ratios2(Product product) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Get ratios - Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("useMeanMatrix","true");
        parameters.put("windowSizeXStr","5");
        parameters.put("windowSizeXStr","5");
        parameters.put("outputSpan","false");
        parameters.put("outputHHVVRatio","true");
        parameters.put("outputHHHVRatio","true");
        parameters.put("outputVVVHRatio","true");

        System.out.println("Computing polarimetric ratios from HH,HV,VH,VV...");
        Product sourceProduct = GPF.createProduct("Polarimetric-Parameters", parameters, product);
        System.out.println("Done.");

        /* Invert ratios - Operator parameters */
        String[] sourceBandNames = sourceProduct.getBandNames();
        for (String s : sourceBandNames){
            System.out.println(s);
        }
        BandMathsOp.BandDescriptor[] targetBands = new BandMathsOp.BandDescriptor[sourceBandNames.length];
        // New bands
        targetBands[0] = new BandMathsOp.BandDescriptor();
        targetBands[0].name = "VVHHRatio";
        targetBands[0].expression = "1/HHVVRatio";
        targetBands[1] = new BandMathsOp.BandDescriptor();
        targetBands[1].name = "HVHHRatio";
        targetBands[1].expression = "1/HHHVRatio";
        targetBands[2] = new BandMathsOp.BandDescriptor();
        targetBands[2].name = "VHVVRatio";
        targetBands[2].expression = "1/VVVHRatio";

        for (BandMathsOp.BandDescriptor targetBand : targetBands){
            targetBand.type = "float32";
            targetBand.noDataValue = sourceProduct.getBandAt(0).getNoDataValue();
        }

        HashMap parameters2 = new HashMap();
        parameters2.put("targetBands",targetBands);
        System.out.println("Inverting ratios to VVHH, HVHH, VHVV...");
        Product outProduct = GPF.createProduct("BandMaths", parameters2, sourceProduct);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Polarimetric ratios VVHH, HVHH, VHVV (" + date + ").\n");
        outProduct.setName(product.getName()+"_polparam");
        return outProduct;
    }

    /* Get TSX ratio (VVHH, HVHH, or VHVV */
    public static Product ratiosTSX(Product product1, Product product2, String ratioName) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Compute TSX ratio - Operator parameters */
        // Combine the two products
        Product combinedProduct = Utils.combine(product1, product2);

        String[] sourceBandNames = combinedProduct.getBandNames();
        for (String s : sourceBandNames){
            System.out.println(s);
        }
        BandMathsOp.BandDescriptor[] targetBands = new BandMathsOp.BandDescriptor[1];
        // New bands
        targetBands[0] = new BandMathsOp.BandDescriptor();
        targetBands[0].name = ratioName;
        targetBands[0].expression = "band1/band2";
        for (BandMathsOp.BandDescriptor targetBand : targetBands){
            targetBand.type = "float32";
            targetBand.noDataValue = combinedProduct.getBandAt(0).getNoDataValue();
        }
        HashMap parameters = new HashMap();
        parameters.put("targetBands",targetBands);

        System.out.println("Computing TSX ratio..."+ratioName);
        Product outProduct = GPF.createProduct("BandMaths", parameters, combinedProduct);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product1.getDescription()+"TSX "+ratioName+" (" + date + ").\n");
        outProduct.setName(product1.getName()+"_ratios");
        return outProduct;
    }

    /* Apply H A alpha decomposition to C3 matrix */
    public static Product HAalpha(Product product) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("decomposition","H-A-Alpha Quad Pol Decomposition");
        // parameters.put("outputAlpha123","true");
        parameters.put("outputHAAlpha","true");
        // parameters.put("outputLambda123","true");
        parameters.put("windowSize","5");

        System.out.println("Applying H A alpha polarimetric decomposition to C3 matrix...");
        Product outProduct = GPF.createProduct("Polarimetric-Decomposition", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"H A alpha decomposition of C3 (" + date + ").\n");
        outProduct.setName(product.getName()+"_decomp");
        return outProduct;
    }
    
    /* Apply Freeman-Durden decomposition to C3 matrix */
    public static Product FreemanDurden(Product product) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters*/
        HashMap parameters = new HashMap();
        parameters.put("decomposition","Freeman-Durden Decomposition");
        parameters.put("windowSize","5");

        System.out.println("Applying Freeman-Durden polarimetric decomposition to C3 matrix...");
        Product outProduct = GPF.createProduct("Polarimetric-Decomposition", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription()+"Freeman-Durden decomposition of C3 (" + date + ").\n");
        outProduct.setName(product.getName()+"_decomp");
        return outProduct;
    }
}
