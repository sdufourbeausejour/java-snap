/**
 *
 * @author dufobeso S. Dufour-BeausÃ©jour PhD student at INRS-ETE QuÃ©bec, Canada
 * s.dufour.beausejour@gmail.com
 *
 * RS2 quad-pol image processing steps:
 - Read RS2 product.xml
 - Create a subset (optional)
 - Apply radiometric calibration
 - Apply speckle filter
 - Compute polarimetric parameters ratios
 - Terrain correction with external DEM
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FilenameUtils;
import java.util.HashMap;
import org.esa.snap.core.dataio.ProductIO;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;


public class ProcessTSX {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    @SuppressWarnings("empty-statement")
    public static void ratios(File imageFile1, File imageFile2, File workingDir, File subsetFile) throws IOException, ParseException, java.text.ParseException {
        /* Input variables :
         imageFile1 (File)   top band (ex. VV in VV/HH)
         imageFile2 (File)   bottom band (ex. HH in VV/HH)
         workingDir (File)   working directory
         subsetFile (File)    subset File */

         String imageName1 = Utils.getImageName(imageFile1);
         System.out.println("Shorter image name is: " + imageName1);

         /* Create output directory (in workingDir) if it doesn't exist */
         File outputDirectory = Utils.getOutputDirectory(workingDir, imageFile1);

         /* Create a readme.txt file if it doesn't exist */
         Utils.createReadme(outputDirectory);
         Product product1 = null;
         Product product2 = null;
         String processingDone = "";

         /* Polarimetric ratios */
         File ratFile = new File(outputDirectory, imageName1 + processingDone + "_sub_rat2.dim");
         Product ratProduct = null;
         if (!(ratFile.exists())) {
             // Read TSX files
             String[] bands1 = {"band1"};
             product1 = ReadProduct.TIFF(imageFile1, bands1);
             String[] bands2 = {"band2"};
             product2 = ReadProduct.TIFF(imageFile2, bands2);

             System.out.println("Subsetting...");
             product1 = Subset.create(product1, subsetFile);
             product2 = Subset.create(product2, subsetFile);

             if (imageFile1.toString().contains("DeceptionBay_orbit13_IncAngle38_HHVV")){
                 ratProduct = Polarimetric.ratiosTSX(product1, product2, "VVHHRatio");
             } else if (imageFile1.toString().contains("DeceptionBay_orbit21_IncAngle40")){
                 ratProduct = Polarimetric.ratiosTSX(product1, product2, "VHVVRatio");
             } else if (imageFile1.toString().contains("DeceptionBay_orbit89_IncAngle46")){
                 ratProduct = Polarimetric.ratiosTSX(product1, product2, "VHVVRatio");
             }

             WriteProduct.BEAMDIMAP(ratProduct, outputDirectory, ratFile);

        }
        processingDone += "_sub_rat2";

        /* GeoTIFF */
        File tiffFile = new File(outputDirectory, imageName1 + processingDone + ".tif");
        //System.out.println("Warning : deleting terrain corrected Tiff product and rewriting it");
        //tiffFile.delete();
        if (!(tiffFile.exists())) {
            product1 = ReadProduct.BEAMDIMAP(ratFile);
            WriteProduct.geoTIFF(product1, tiffFile);
            // product.dispose();
        }
    }

    public static void texture(File imageFile1, File imageFile2, File workingDir, File subsetFile) throws IOException, ParseException, java.text.ParseException {
        /* Input variables :
         imageFile1 (File)   top band (ex. VV in VV/HH)
         imageFile2 (File)   bottom band (ex. HH in VV/HH)
         workingDir (File)   working directory
         subsetFile (File)    subset File */

         String imageName1 = Utils.getImageName(imageFile1);
         System.out.println("Shorter image name is: " + imageName1);

         /* Create output directory (in workingDir) if it doesn't exist */
         File outputDirectory = Utils.getOutputDirectory(workingDir, imageFile1);

         /* Create a readme.txt file if it doesn't exist */
         Utils.createReadme(outputDirectory);
         Product product = null;
         Product product1 = null;
         Product product2 = null;
         String processingDone = "";

         /* Texture */
         File texFile = new File(outputDirectory, imageName1 + processingDone + "_sub_tex2.dim");
         Product texProduct1 = null;
         Product texProduct2 = null;
         if (!(texFile.exists())) {
             // Read TSX files, renaming bands and setting fake unit for GLCM
             String[] bands1 = new String[1];
             String[] bands2 = new String[1];
             if (imageFile1.toString().contains("DeceptionBay_orbit13_IncAngle38_HHVV")){
                 bands1[0] = "VV";
                 bands2[0] = "HH";
             } else if (imageFile1.toString().contains("DeceptionBay_orbit21_IncAngle40")){
                 bands1[0] = "VH";
                 bands2[0] = "VV";
             } else if (imageFile1.toString().contains("DeceptionBay_orbit89_IncAngle46")){
                 bands1[0] = "VH";
                 bands2[0] = "VV";
             }
             product1 = ReadProduct.TIFF(imageFile1, bands1);
             product1.getBandAt(0).setUnit("Intensity");
             product2 = ReadProduct.TIFF(imageFile2, bands2);
             product2.getBandAt(0).setUnit("Intensity");

            /* Subset the products */
             System.out.println("Subsetting...");
             product1 = Subset.create(product1, subsetFile);
             product2 = Subset.create(product2, subsetFile);

            /* Texture */
            // String sourceBands = String.join(",", product.getBandNames());
            texProduct1 = Texture.GLCM(product1, bands1[0]);
            texProduct2 = Texture.GLCM(product2, bands2[0]);

            /* Combine the two products*/
            product = Utils.combine(texProduct1, texProduct2);
            WriteProduct.BEAMDIMAP(product, outputDirectory, texFile);
        }
        processingDone += "_sub_tex2";

        /* GeoTIFF */
        File tiffFile = new File(outputDirectory, imageName1 + processingDone + ".tif");
        //System.out.println("Warning : deleting terrain corrected Tiff product and rewriting it");
        //tiffFile.delete();
        if (!(tiffFile.exists())) {
            product = ReadProduct.BEAMDIMAP(texFile);
            WriteProduct.geoTIFF(product, tiffFile);
            // product.dispose();
        }
    }
}
