/*
 * Apply speckle filter
 * - LeeSigma : filters product using the Lee Sigma 7x7 speckle filter (Product)
 * - PolarimetricRefinedLee: creates and returns product filtered by the polarimetric Refined Lee 7x7 speckle filter (Product)
 */
package com.batchprocessing.java.snap;

import java.io.File;
import java.util.HashMap;
import org.esa.snap.core.gpf.GPF;
import org.esa.snap.core.datamodel.Product;

/**
 *
 * @author dufobeso S. Dufour-Beauséjour PhD student at INRS-ETE Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class SpeckleFilter {

    /* Apply Lee Sigma filter */
    public static Product LeeSigma(Product product) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        HashMap parameters = new HashMap();
        parameters.put("filter", "Lee Sigma");
        parameters.put("windowSize", "7x7");
        // Many other parameters are set to default : filterSize (3), number of looks (1), sigma (0.9), targetWindowSize (3x3)

        System.out.println("Applying Lee Sigma 7x7 Speckle Filter");
        Product outProduct = GPF.createProduct("Speckle-Filter", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription() + "Product filtered with Lee Sigma 7x7 (" + date + ").\n");
        outProduct.setName(product.getName() + "_spk");
        return outProduct;
    }

    /* Apply polarimetric Refined Lee filter */
    public static Product PolarimetricRefinedLee(Product product) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        HashMap parameters = new HashMap();
        parameters.put("filter", "Refined Lee Filter");
        parameters.put("windowSize", "7x7");
        // Many other parameters are set to default : filterSize (5), number of looks (1), sigma (0.9), targetWindowSize (3x3)

        System.out.println("Applying polarimetric Refined Lee 7x7 Speckle Filter");
        Product outProduct = GPF.createProduct("Polarimetric-Speckle-Filter", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription() + "Product filtered with polarimetric Refined Lee 7x7 (" + date + ").\n");
        outProduct.setName(product.getName() + "_spk");
        return outProduct;
    }
}
