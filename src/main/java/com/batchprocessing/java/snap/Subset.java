/*
 * Create an image subset
 * - create: subset product using input shapefile (Product)
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import java.io.IOException;
import com.vividsolutions.jts.io.WKTReader;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import org.esa.snap.core.datamodel.Band;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author dufobeso S. Dufour-Beauséjour PhD student at INRS-ETE Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Subset {
    /* Create the subset */
    public static Product create(Product product, File subsetFile) throws ParseException, IOException {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        // Get extent of shapefile
        Map map = new HashMap();
        map.put( "url", subsetFile.toURL() );
        DataStore dataStore = DataStoreFinder.getDataStore(map);
        SimpleFeatureSource featureSource = dataStore.getFeatureSource(FilenameUtils.removeExtension(subsetFile.getName()));
        SimpleFeatureCollection collection = featureSource.getFeatures();
        ReferencedEnvelope env = collection.getBounds();
        double left = env.getMinX();
        System.out.println(Double.toString(left));
        double right = env.getMaxX();
        double top = env.getMaxY();
        double bottom = env.getMinY();

        // Create rectangular WKT geometry from shapefile extent
        Geometry subsetGeometry = new WKTReader().read("POLYGON (("+left+" "+top+","+right+" "+top+","+right+" "+bottom+","+left+" "+bottom+","+left+" "+top+"))");

        /* Operator parameters */
        HashMap parameters = new HashMap();
        parameters.put("copyMetadata", "True");
        parameters.put("geoRegion", subsetGeometry);
        System.out.println("Subset is: " + subsetFile.getName());
        System.out.println("Creating subset ...");

        // Create product
        Product outProduct = GPF.createProduct("Subset", parameters, product);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(product.getDescription() + "Product subset created (" + date + ").\n");
        outProduct.setName(product.getName() + "_sub");

        return outProduct;
    }
}
