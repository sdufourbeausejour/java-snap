/*
 * Utilities :
 * - addTrainginVectors : Add training vectors for bay (Hashmap)
 * - copyOriginalMetadata : copy metadata from original product to processed product (Product)
 * - createDirectory : Creates a new directory if it doesn't exist (File)
 * - createInSARWorkingDirectory : Creates and returns the workingDirectory for InSAR (File)
 * - createMultiTemporalWorkingDirectory : Creates and returns the workingDirectory for MultiTemporal (File)
 * - createReadme: Creates a readme file with header if it doesn't exist ()
 * - createWorkingDirectory: Creates and returns the workingDirectory (File)
 * - deleteBEAMDIMAP : Deletes the .dim file and .data folder associated with a BEAM-DIMAP image product ()
 * - deleteDir: Deletes the directory and all its content ()
 * - deleteSuperfluousVectors : Deletes all water vectors except _1 ()
 * - displayFiles: Print files in file array ()
 * - filterFileArray : Filter file array using regex (File[])
 * - findMatchingImages: Match string pattern to files in directory and return file array of matches (File[])
 * - getBandNames : Returns band names for TSX series (String[])
 * - getBay: Returns the name of the bay based on the date; must be updated with acquisistions (String)
 * - getBayPattern : Returns the pattern of dates matching the input bayName (String)
 * - getCollapsedImageStackName : Returns the collapsed image stack name with begin and end imageDates (String)
 * - getClassifDirectory : Returns the classif directory name from classifier bands (String)
 * - getDirectory : Returns the directory containing all images from given sensor (File)
 * - getDualType : Returns the string for the dual-pol type : HH/HV, VH/VV, HH/VV from readme (String)
 * - getImageStackName : Returns the image stack name from an array of image files (String)
 * - getImageName : Returns a shorter imageName (String)
 * - getKennaughRGB : Returns a string with the list of Kennaugh elements in RGB order (String)
 * - getProductXML : Returns the TSX product .xml file needed to read the product (File)
 * - getPolarizationNames : Returns a string array of the name of image polarizations (String[])
 * - getROIVectorName : Returns ROI vector name from bayName, for masking (String)
 * - getSeriesName : Returns TSX seriesName from imageFile (String)
 * - getSourceBands : Returns a String with the source band names such as "i_HH,q_HH,i_VV,q_VV" (String)
 * - getTSXseriesDirectory : Returns the seriesDirectory for TSX data (String)
 * - getWorkingDirectory : Returns the workingDirectory if it exists (File)
 * - writeDualType : Writes the dual type from origin product bands to the readme ()
 *
 * - addVector: Add a vector without seperating the shapes (Product)
 * - combine : merge two products into one (Product)
 * - getOutputDirectory : Returns the outputDirectory, creating it if necessary (File)
 * - getReadableTime : Returns human readable time (hours, minutes, etc) from nanotime (String)
 * - getTimeStamp : Returns the time yyyy/MM/dd HH:mm:ss (String)
 * - updateReadme : Returns the readme (File)
 */
package com.batchprocessing.java.snap;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.RegexFileFilter;
import org.esa.snap.core.datamodel.MetadataAttribute;
import org.esa.snap.core.datamodel.MetadataElement;
import org.esa.snap.core.datamodel.Product;
import org.esa.snap.core.gpf.GPF;

/**
 *
 * @author dufobeso S. Dufour-Beauséjour PhD student at INRS-ETE Québec, Canada
 * s.dufour.beausejour@gmail.com
 */
public class Utils {

    public static HashMap addTrainingVectors(Product product, File imageFile, String[] types) {

        // Import training area vectors for this bay
        String bayName = getBay(imageFile);

        FileFilter bayFilter = new RegexFileFilter(bayName + ".*shp");
        File[] trainingFiles = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\Training\\\\").listFiles(bayFilter);
        System.out.println("All files and directories:");
        Utils.displayFiles(trainingFiles);

        File shapeFile = null;
        String[] trainingVectors = new String[types.length];
        int n = 0;
        String typesTogether = null;
        for (String type : types) {
            typesTogether += type + "  ";
            // Find vector name matching the ice type
            String vectorName = null;
            for (File trainingFile : trainingFiles) {
                String trainingFileName = trainingFile.getName();
                Pattern typePattern = Pattern.compile("(" + bayName + type + ".*)\\.shp");
                Matcher vectorMatcher = typePattern.matcher(trainingFileName);

                if (vectorMatcher.find()) {
                    vectorName = vectorMatcher.group(1);
                    System.out.println("Matched vectorName : " + vectorName);
                    break;
                } else {
                    continue;
                }
            }

            shapeFile = new File("Z:\\\\Donnees\\\\Imagerie\\\\VectorData\\\\Training\\\\" + vectorName + ".shp");
            System.out.println("ShapeFile name : " + shapeFile.getName());
            if (shapeFile.exists()) {
                product = addVector(product, imageFile, shapeFile);
                trainingVectors[n] = vectorName + "_1";
                n++;
            }
        }
        HashMap map = new HashMap();
        map.put("product", product);
        map.put("vectorNames", trainingVectors);

        return map;
    }

    public static Product copyOriginalMetadata(Product product, File originalImageFile) throws IOException {
        /* Read the original TSX product */
        Product originalProduct = ReadProduct.TSX(originalImageFile);

        // Get Abstracted_Metadata from originalProduct
        MetadataElement originalElement = originalProduct.getMetadataRoot().getElement("Abstracted_Metadata");
        // Add to processed product and get as oldElement
        product.getMetadataRoot().addElement(originalElement);
        MetadataElement oldElement = product.getMetadataRoot().getElement("Abstracted_Metadata");

        // For every attribute in the original element, remove its counterpart in the processed product and replace it
        // with the original element attribute
        for (MetadataAttribute attribute : product.getMetadataRoot().getElement("Abstracted_Metadata").getAttributes()) {
            System.out.println(attribute.getName());
            MetadataAttribute originalAttribute = originalElement.getAttribute(attribute.getName());

            oldElement.removeAttribute(attribute);
            oldElement.addAttribute(originalAttribute);
            System.out.println(oldElement.getAttributeString(attribute.getName()));
        }
        // For every subElement in the original element, add it to the processed product element,
        // and add each of its attributes
        for (MetadataElement subElement : oldElement.getElements()) {
            System.out.println(subElement.getName());
            // Get new subElement from original product
            MetadataElement originalSubElement = originalElement.getElement(subElement.getName());

            oldElement.removeElement(subElement);
            oldElement.addElement(originalSubElement);
        }
        return product;
    }

    /* Create directory if it doesn't exist*/
    public static File createDirectory(File workingDirectory, String directoryName) {

        File newDirectory = new File(workingDirectory, directoryName);

        if (!(newDirectory.isDirectory())) {
            newDirectory.mkdir();
            System.out.println("New directory \"" + newDirectory.getPath() + "\" created.");
        }
        return newDirectory;
    }

    /* Create the InSAR working directory if it doesn't exist*/
    public static File createInSARWorkingDirectory(File[] imageFiles) {
        /* Fetch image stack name */
        String imageStackName = getImageStackName(imageFiles);

        /* From image stack name, create working directory if not already created */
        File workingDirectory = new File(imageFiles[0].getParent(), "InSAR" + File.separator + imageStackName);

        if (!(workingDirectory.isDirectory())) {
            workingDirectory.mkdir();
            System.out.println("Working directory \"" + workingDirectory.getPath() + "\" created.");
        }
        return workingDirectory;
    }

    /* Create the MultiTemporal working directory if it doesn't exist*/
    public static File createMultiTemporalWorkingDirectory(File multiTemporalDirectory, String collapsedImageName) {
        /* From collapsedImageName and globalDirectory, create working directory if not already created */
        File workingDirectory = new File(multiTemporalDirectory, collapsedImageName + File.separator);

        if (!(workingDirectory.isDirectory())) {
            workingDirectory.mkdir();
            System.out.println("Working directory \"" + workingDirectory.getPath() + "\" created.");
        }
        return workingDirectory;
    }

    /* Create the working directory if it doesn't exist*/
    public static File createWorkingDirectory(File imageFile) {
        /* From shortened image name, create working directory if not already created */
        String imageName = getImageName(imageFile);

        File workingDirectory = new File(imageFile.getParent(), imageName);

        if (!(workingDirectory.isDirectory())) {
            workingDirectory.mkdir();
            System.out.println("Working directory \"" + workingDirectory.getPath() + "\" created.");
        }
        return workingDirectory;
    }

    /* Delete BEAM-DIMAP product */
    public static void deleteBEAMDIMAP(File dimFile) {
        dimFile.delete();
        File dataFile = new File(dimFile.getParent(), FilenameUtils.getBaseName(dimFile.getName()) + ".data");
        Utils.deleteDir(dataFile);
    }

    /* Delete folder */
    public static void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }

    /* Delete unwanted vectors */
    public static void deleteSuperfluousVectors(File imageFile, String processingDone) {
        String imageName = getImageName(imageFile);
        File workingDirectory = new File(imageFile.getParent(), imageName);
        // Fetch vector list
        File directory = new File(workingDirectory, imageName + processingDone + ".data\\\\vector_data\\\\");
        File[] files = directory.listFiles();

        String pattern = ("(Water|mask).*csv");
        FileFilter filter = new RegexFileFilter(pattern);

        files = directory.listFiles(filter);
        for (File f : files) {
            f.delete();
        }
    }

    /* Display (print) all files in directory*/
    public static void displayFiles(File[] files) {
        for (File file : files) {
            System.out.println(file.getName());
        }
    }

    /* Filter file array with regex */
    public static File[] filterFileArray(File[] files, String pattern) {

        List<File> matches = new ArrayList<File>();

        String fileName = null;
        for (File file : files) {
            fileName = file.getName();
            if (fileName.matches(pattern)) {
                matches.add(file);
            }
        }

        File[] matchingFiles = matches.toArray(new File[matches.size()]);
        return matchingFiles;
    }

    /* Find matching images */
    public static File[] findMatchingImages(File directory, String pattern) throws IOException {

        File[] files = directory.listFiles();
        //System.out.println("All files and directories:");
        //displayFiles(files);

        //String pattern = "[tT]est[1-3].txt";
        System.out.println("\nFiles that match regular expression: " + pattern);
        FileFilter filter = new RegexFileFilter(pattern);
        files = directory.listFiles(filter);

        Arrays.sort(files);
        displayFiles(files);
        return files;
    }

    /* Get band names for TSX image series */
    public static String[] getBandNames(String seriesName) {
        String[] bandNames = null;
        if (seriesName.equals("DeceptionBay_orbit21_IncAngle40_Processed")) {
            bandNames = new String[]{"K0", "K1", "K5", "K8"};
        } else {
            System.out.println("Unexpected seriesName in Utils.getBandNames");
        }
        return bandNames;
    }

    /* Get bay name */
    public static String getBay(File imageFile) {

        /* Correspondance between acquisition dates and bays; will have to be updated with every acquisition. */
        String patternSalluit = getBayPattern("Salluit");
        String patternKangiqsujuaq = getBayPattern("Kangiqsujuaq");
        String patternDeceptionBay = getBayPattern("DeceptionBay");

        String imageName = Utils.getImageName(imageFile);

        String bayName = new String();
        /* Try matching imageName to expected dates and thus identify the bay. */
        if (imageName.matches(patternSalluit.substring(0, patternSalluit.length() - 11))) {
            bayName = "Salluit";
        } else if (imageName.matches(patternKangiqsujuaq.substring(0, patternKangiqsujuaq.length() - 11))) {
            bayName = "Kangiqsujuaq";
        } else if (imageName.matches(patternDeceptionBay.substring(0, patternDeceptionBay.length() - 11))) {
            bayName = "DeceptionBay";
        } else {
            System.out.println("Could not match imageName to expected date and thus could not identify the bay.");
            // This should cause the processing to stop, but for now it doesn't.
        }

        /* Return geometry string */
        return bayName;
    }

    public static String getBayPattern(String bayName) {
        String pattern = null;

        if (bayName.contains("Salluit")) {
            pattern = (".*(20151219|(2016(0112|0205|0229|0324|0417|1213))|(2017(0106|0130|0223|0319|0412|1208))|(2018(0101|0125|0218|0407|0501))).*(SLC|SLC_1)");
        } else if (bayName.contains("Kangiqsujuaq")) {
            pattern = (".*(20151223|(2016(0116|0209|0304|0328|0421|1217))|(2017(0110|0203|0227|0323|0416|1212))|(2018(0105|0129|0222|0411|0505|0529))).*(SLC|SLC_1)");
        } else if (bayName.contains("DeceptionBay")) {
            pattern = (".*(20151226|(2016(0119|0212|0307|0331|0424|1220))|(2017(0113|0206|0302|0326|0419|1215))|(2018(0108|0201|0225|0414|0508))).*(SLC|SLC_1)");
        }

        return pattern;
    }

    public static String getClassifDirectory(String[] classifierBands) {
        String classifDirectory = "";
        for (String s : classifierBands) {
            classifDirectory += s;
            classifDirectory += "_";
        }
        classifDirectory = classifDirectory.substring(0, classifDirectory.length() - 1);
        return classifDirectory;
    }

    /* Get collapsed image stack name from imageFiles : first and last image dates */
    public static String getCollapsedImageStackName(File[] imageFiles) {
        /* Get the dates for the beginning and end of the series */
        String firstImageName = Utils.getImageName(imageFiles[0]);
        int minDate = Integer.parseInt(firstImageName.substring(4));
        int maxDate = minDate;

        for (File imageFile : imageFiles) {
            String imageName = Utils.getImageName(imageFile);
            int imageDate = Integer.parseInt(imageName.substring(4));
            if (imageDate < minDate) {
                minDate = imageDate;
            } else if (imageDate > maxDate) {
                maxDate = imageDate;
            }
        }

        /* Image stack name = sensorName_minDate_maxDate */
        String imageStackName = firstImageName.substring(0, 3) + "_" + Integer.toString(minDate) + "_" + Integer.toString(maxDate);
        return imageStackName;
    }

    /* Get the directory containing all images (either RS2 or TSX) */
    public static File getDirectory(String sensor) {
        File directory = null;
        if (sensor.contains("RADARSAT-2")) {
            directory = new File("Z:\\\\Donnees\\\\Imagerie\\\\RADARSAT-2\\\\");
        } else if (sensor.contains("TerraSAR-X")) {
            directory = new File("Z:\\\\Donnees\\\\Imagerie\\\\TerraSAR-X\\\\");
        } else if (sensor.contains("MacRS2")) {
            directory = new File("C:\\\\Users\\\\dufobeso\\\\Desktop\\\\RS2_Analysis\\\\Images");
        } else if (sensor.contains("ExternalDriveTSX")) {
            directory = new File("F:\\\\Doctorat\\\\BaieDeception\\\\Donnees\\\\Imagerie\\\\TerraSAR-X\\\\");
        }
        return directory;
    }

    /* Get the type of dual pol : VH/VV, HH/HV... from readme */
    public static String getDualType(File imageFile) throws IOException {
        String dualType = null;
        File workingDirectory = Utils.getWorkingDirectory(imageFile);
        File readmeFile = Utils.updateReadme(workingDirectory);

        Scanner scanner = new Scanner(readmeFile);

        boolean foundDualType = false;
        int lineNum = 0;
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            lineNum++;
            if (line.contains("Dual type")) {
                dualType = line.substring(15, 21);
                System.out.println("Dual type is : " + dualType);
                foundDualType = true;
                break;
            }
        }
        if (!foundDualType) {
            System.out.println("Failed to find dualType in the readme file.");
        }
        return dualType;
    }

    /* Return the image stack name for InSAR   */
    public static String getImageStackName(File[] imageFiles) {
        String imageStackName = Utils.getImageName(imageFiles[0]).substring(0, 3);
        for (File imageFile : imageFiles) {
            imageStackName += "_" + Utils.getImageName(imageFile).substring(4);
        }

        return imageStackName;
    }

    /* Get quickfile for Kennaugh RGB composite */
    public static String getKennaughRGB(String seriesName) {
        String KennaughRGB = null;
        if (seriesName.equals("DeceptionBay_orbit21_IncAngle40_Processed")) {
            KennaughRGB = "K1_K0_K5";
        } else {
            System.out.println("Unexpected seriesName in Utils.getKennaughRGB.");
        }
        return KennaughRGB;
    }

    /* Get original image file from imageFile and seriesName for TSX */
    public static File getOriginalTSXImageFile(File imageFile, String seriesName) throws IOException {
        /* Get original series name, directory */
        String originalSeriesName = seriesName.split("_Processed")[0];
        System.out.println("original series : " + originalSeriesName);
        File globalDirectory = Utils.getDirectory("TerraSAR-X");
        String originalSeriesDirectory = Utils.getTSXseriesDirectory(originalSeriesName);
        File directory = new File(globalDirectory, originalSeriesDirectory);

        /* Find originalImageFile from match to subset imageName */
        String imageName = Utils.getImageName(imageFile);
        String pattern = ("TSX1.*" + imageName.substring(4) + ".*");
        File originalImageFile = Utils.findMatchingImages(directory, pattern)[0];

        System.out.println("Matched imageFile : ");
        System.out.println(imageFile.toPath().getFileName());
        System.out.println("to originalImageFile : ");
        System.out.println(originalImageFile.toPath().getFileName());

        return originalImageFile;
    }

    /* Get product xml file */
    public static File getProductXML(File imageFile) throws IOException {

        System.out.print("Retrieving product xml...");
        File[] files = Utils.findMatchingImages(imageFile, ".*.xml");

        return files[0];
    }

    /* Return the image polarizations (HH,HV,VH,VV) as a String array */
    public static String getPolarizationNames(Product product) {
        String[] bandNames = product.getBandNames();

        String polarizationNames = "";
        for (String bandName : bandNames) {
            if ((bandName.contains("Intensity"))) {
                polarizationNames += bandName.substring(10);
                polarizationNames += ",";
            }
        }

        return polarizationNames.substring(0, 5);
    }

    public static String getROIVectorName(String bayName){
        String ROIVectorName = null;
        if (bayName == "Salluit"){
            ROIVectorName = "maskSalluit_strait_MultiPolygon_1";
        } else if (bayName == "Kangiqsujuaq"){
            ROIVectorName = "maskKangiqsujuaq_strait_MultiPolygon_1";
        } else if (bayName == "DeceptionBay"){
            ROIVectorName = "maskDeceptionBay_Polygon_1";
        }
        return ROIVectorName;
    }

    public static String getSeriesName(File imageFile) {
        String seriesName = imageFile.getParent();
        System.out.println("seriesName is : " + seriesName);
        return seriesName;
    }

    /* Return the source bands of polarimetric product as a String */
    public static String getSourceBands(Product product) {
        String[] bandNames = product.getBandNames();
        String sourceBands = "";
        for (String bandName : bandNames) {
            if (!(bandName.contains("Intensity"))) {
                sourceBands += bandName;
                sourceBands += ",";
            }
        }
        return sourceBands;
    }


    /* TSX data is organized by bay and orbit */
    public static String getTSXseriesDirectory(String seriesName) {
        String seriesDirectory = null;
        if (seriesName.equals("DeceptionBay_orbit21_IncAngle40_Processed")) {
            seriesDirectory = "DeceptionBay_orbit21_IncAngle40_Processed\\\\";
        } else if (seriesName.contains("DeceptionBay_orbit21")) {
            seriesDirectory = "DeceptionBay_orbit21_IncAngle40\\\\";
        } else if (seriesName.contains("DeceptionBay_orbit89")) {
            seriesDirectory = "DeceptionBay_orbit89_IncAngle46\\\\";
        }
        return seriesDirectory;
    }

    /* Get the working directory*/
    public static File getWorkingDirectory(File imageFile) {

        /* From shortened image name, create working directory if not already created */
        String imageName = getImageName(imageFile);

        File workingDirectory = new File(imageFile.getParent(), imageName);

        if (!(workingDirectory.isDirectory())) {
            System.out.println("Working directory \"" + workingDirectory.getPath() + "\" doesn't exist.");
        }
        System.out.println("Working directory is " + workingDirectory.getAbsolutePath());
        return workingDirectory;
    }


    /* Get the type of dual pol : VH/VV, etc from original product and write dualType to readme file*/
    public static void writeDualType(Product product, File imageFile) throws IOException {
        String dualType = null;
        Product originalProduct = ReadProduct.TSX(imageFile);
        String sourceBands = Utils.getSourceBands(originalProduct);
        if (sourceBands.contains("HH") && sourceBands.contains("HV")) {
            dualType = "HH/HV";
        } else if (sourceBands.contains("VH") && sourceBands.contains("VV")) {
            dualType = "VH/VV";
        } else if (sourceBands.contains("HH") && sourceBands.contains("VV")) {
            dualType = "HH/VV";
        } else {
            System.out.println("Unexpected sourceBands.");
        }

        File workingDirectory = Utils.getWorkingDirectory(imageFile);
        File readmeFile = Utils.updateReadme(workingDirectory);
        String date = Utils.getTimeStamp();

        try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile, true), StandardCharsets.UTF_8))) {
            readme.append("Dual type is : " + dualType + "  (" + date + ")\n");
        }

    }



    /* Import shapefile as vector without seperating shapes */
    public static Product addVector(Product product, File imageFile, File shapeFile) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        HashMap parameters = new HashMap();

        parameters.put("vectorFile", shapeFile);
        parameters.put("seperateShapes", "False");

        // Imports vector shapefile into product
        Product outProduct = GPF.createProduct("Import-Vector", parameters, product);

        return outProduct;
    }

    public static Product combine(Product masterProduct, Product sourceProduct) {
        GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis();

        /* Operator parameters */
        HashMap parameters = new HashMap();
        /* Operator sourceProducts */
        HashMap sources = new HashMap();
        sources.put("masterProduct", masterProduct);
        sources.put("sourceProducts",sourceProduct);

        System.out.print("Merging ");
        System.out.print(sourceProduct.getName()+" ");
        System.out.print(" with "+masterProduct.getName()+"...");

        Product outProduct = GPF.createProduct("Merge", parameters, sources);
        System.out.println("Done.");

        /* Update product description and name */
        String date = Utils.getTimeStamp();
        outProduct.setDescription(masterProduct.getDescription()+"Combined products (" + date + ").\n");
        outProduct.setName(masterProduct.getName()+"_combined");
        return outProduct;
    }

    /* Create readme.txt file in output directory if it doesn't already exist. Returns a readme File. */
    public static void createReadme(File outputDirectory) throws IOException {
        File readmeFile = new File(outputDirectory, "readme.txt");

        if (!(readmeFile.exists())) {
            try (BufferedWriter readme = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(readmeFile), StandardCharsets.UTF_8))) {
                readme.write("Image processing log created by java-snap https://gitlab.com/sdufourbeausejour/java-snap\n"
                        + "For questions or comments about java-snap, contact Sophie Dufour-Beauséjour s.dufour.beausejour@gmail.com \n"
                        + "java-snap uses the satellite image processing tool called SNAP, developped by ESA http://step.esa.int/main/toolboxes/snap/ \n \n"
                        + "Analysis steps : \n");
            }
            System.out.println("Created a readme.txt file.");
        }
    }

    /* Get a shorter image name from the imageFile */
    public static String getImageName(File imageFile) {
        /* From imageFile, get shorter imageName ex. RS2_20151219 */
        String longImageName = new String(imageFile.getParentFile().getName());
        String imageName = null;
        if (imageFile.toString().contains("RS2")) {
            Pattern datePattern = Pattern.compile(".*(\\d{8}+)_.*(SLC|SLC_1)");
            Matcher dateMatcher = datePattern.matcher(longImageName);

            if (dateMatcher.find()) {
                imageName = "RS2_" + dateMatcher.group(1);
            } else {
                imageName = "Could not find match between image name and regex.";
            }
        } else if (imageFile.getName().contains("K0001")) {
            System.out.println(imageFile.getName());
            Pattern datePattern = Pattern.compile(".*K0001_(\\d{8}+).*");
            Matcher dateMatcher = datePattern.matcher(imageFile.getName());

            if (dateMatcher.find()) {
                imageName = "TSX_" + dateMatcher.group(1);
            } else {
                imageName = "Could not find match between image name and regex.";
            }
        } else if (imageFile.toString().contains("TSX_db")) {
            Pattern datePattern = Pattern.compile(".*_(\\d{8}+).*TSX_db.tif");
            Matcher dateMatcher = datePattern.matcher(longImageName);

            if (dateMatcher.find()) {
                imageName = "TSX_" + dateMatcher.group(1);
            } else {
                imageName = "Could not find match between image name and regex.";
            }
        } else {
            imageName = "Could not identify sensor in Utils.getImageName";
        }
        return imageName;
    }

    /* Get readable time from nanoseconds */
    public static String getReadableTime(Long nanos) {
        long tempSec = nanos / (1000 * 1000 * 1000);
        long sec = tempSec % 60;
        long min = (tempSec / 60) % 60;
        long hour = (tempSec / (60 * 60)) % 24;
        long day = (tempSec / (24 * 60 * 60)) % 24;
        return String.format("%dd %dh %dm %ds", day, hour, min, sec);
    }

    /* Return time stamp to be used in readme update */
    public static String getTimeStamp() {
        DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date today = Calendar.getInstance().getTime();
        String timeStamp = df.format(today);
        return timeStamp;
    }

    /* Returns readme.txt file and warns if it hasn't been created yet. */
    public static File updateReadme(File outputDirectory) throws IOException {
        File readmeFile = new File(outputDirectory, "readme.txt");
        if (!(readmeFile.exists())) {
            System.out.println("readme.txt does not exist.");
        }
        return readmeFile;
    }

    /* Get the output directory, create it if it doesn't exist*/
    public static File getOutputDirectory(File workingDir, File imageFile) {
        /* From shortened image name, create output directory */
        String imageName = getImageName(imageFile);
        File outputDirectory = new File(workingDir, imageName);
        if (!(outputDirectory.isDirectory())) {
            outputDirectory.mkdir();
            System.out.println("Output directory \"" + outputDirectory.getPath() + "\" created.");
        }
        return outputDirectory;
    }
}
