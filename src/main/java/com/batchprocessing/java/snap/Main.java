/**
 * @author dufobeso
 * Sophie Dufour-Beausejour
 * PhD student at INRS-ETE
 * Quebec, Canada
 * s.dufour.beausejour@gmail.com
 *
 * This java project focuses on processing RADARSAT-2 fully polarimetric (quad-pol) images.
 * Written in the java language, it relies on
 * [snap-engine] (https://github.com/senbox-org/snap-engine) and
 * [s1tbx](https://github.com/senbox-org/s1tbx) modules,
 * developped by ESA as part of their [SNAP application](http://step.esa.int/main/toolboxes/snap/).
 *
 */
package com.batchprocessing.java.snap;

import com.vividsolutions.jts.io.ParseException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    public static void Process(String[] args) throws IOException, ParseException, java.text.ParseException {
        /* Expected arguments (given as a white-space seperated list of strings)
        for this Process are:
        0 : processName (string): name of process to use
            ex. ProcessRS2
        1 : inputImageList (path): absolute path to text file with a list of absolute paths
            to product.xml of images to process
            ex. /C/Users/dufobeso/Documents/working_dir/images.txt
        2 : workingDir (path): absolute path to the working directory where
            processing outputs should be written.
            ex. /C/Users/dufobeso/Documents/working_dir
        3 : subsetPath (string): absolute path to subset shapefile, or "None"
            ex. /C/Users/dufobeso/Documents/working_dir/ROI.shp
        4 : DEMPath (string): absolute path to DEM file, or "None"
            ex. /C/Users/dufobeso/Documents/working_dir/dem.tif
        5 : maskPath (string): absolute path to mask shapefile, or "None"
            ex. /C/Users/dufobeso/Documents/working_dir/mask.shp

        example of command line call (remove line breaks) :
        $ java -jar java-snap-1.0.jar
          ProcessRS2
          /Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/working_dir_RS2/Salluit_RS2_test_path.txt
          /Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/working_dir_RS2
          /Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/VectorData/ROI/maskSalluit_Polygon.shp
          /Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/DEM/mnt2.tif
          /Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/VectorData/special_issue_TSX/Water_HudsonBay_1_MultiPolygon.shp
         */

        /* Distribute the input arguments to variables */
        String processName = args[0];
        File imageListFile = new File(args[1]);
        File workingDir = new File(args[2]);
        File subsetFile = null;
        File DEMFile = null;
        File maskFile = null;
        if (!args[3].contains("None")){
            subsetFile = new File(args[3]);
        }
        if (!args[4].contains("None")){
            DEMFile = new File(args[4]);
        }
        if (!args[5].contains("None")){
            maskFile = new File(args[5]);
        }

        /* Print out workingDir where processed images will be written*/
        System.out.println("Working directory is: " + workingDir.getAbsolutePath());

        /* Read the inputImagesList file line by line, adding them to an
        ArrayList of strings */
        System.out.println("Reading list of images to process: " + imageListFile.getAbsolutePath());
        Scanner inFile = new Scanner(imageListFile);
        List<String> lines = new ArrayList<String>();
        while (inFile.hasNext()) {
            lines.add(inFile.nextLine());
        }
        /* From every line, define a File associated with the image listed,
        add it to imageFiles and print it */
        System.out.println("Image files to process are:");
        File[] imageFiles = new File[lines.size()];
        File[] imageFiles2 = new File[lines.size()];

        int i = 0;
        for (String line : lines) {
            String[] parts = line.split(";");
            imageFiles[i] = new File(parts[0]);
            if (processName.contains("ProcessTSX")){
                imageFiles2[i] = new File(parts[1]);
                System.out.println(imageFiles[i].getName());
            } else {
                System.out.println(imageFiles[i].getParentFile().getName());                
            }
            i = i + 1;
        }

        /* Process images one by one: */
        int n = imageFiles.length;
        System.out.println(n + " images to process.");
        int k = 1;
        for (File imageFile : imageFiles) {
            System.out.println("Image number " + k + " out of " + n + "...");
            String[] outerArgs = new String[]{imageFile.toString()};

            /* Launch a specific processing chain */
            if (processName.contains("RS2")){
                if (processName.contains("HAa")){
                    ProcessRS2HAa.main(imageFile, workingDir, subsetFile, DEMFile, maskFile);
                } else if (processName.contains("ratios")) {
                    ProcessRS2ratios.main(imageFile, workingDir, subsetFile, DEMFile, maskFile);
                } else if (processName.contains("quicklook")) {
                    ProcessRS2quicklook.main(imageFile, workingDir, subsetFile, DEMFile, maskFile);
                } else if (processName.contains("postTC")) {
                    ProcessRS2texture.postTC(imageFile, workingDir);
                } else if (processName.contains("preTC")) {
                    ProcessRS2texture.preTC(imageFile, workingDir, DEMFile);
                } else {
                    System.out.println("Warning: unexpected processName : " + processName);
                }
            } else if (processName.contains("TSX")){
                if (processName.contains("ratios")) {
                    ProcessTSX.ratios(imageFile, imageFiles2[k-1], workingDir, subsetFile);
                } else if (processName.contains("texture")) {
                    ProcessTSX.texture(imageFile, imageFiles2[k-1], workingDir, subsetFile);
                }
            }
            k = k + 1;
        }
    }

    public static void main(String[] args) throws IOException, ParseException, java.text.ParseException {
        /* Expected arguments (given as a white-space seperated list of strings)
           for this Process are :
        0 : processName (String) : name of processing to conduct
            ex. ProcessRS2
        1-N : arguments specific for processing, listed above

        /* Fetch and print the time at the start of the processing */
        long startTime = System.nanoTime();
        Date date = new Date();
        System.out.println("Processing started at " + date.toString());

        /* Launch processing chain */
        Process(args);

        /* Fetch and print the time at the end of the processing, as well as the
        elapsed time */
        System.out.println("Processing ended at " + date.toString());
        long stopTime = System.nanoTime();
        System.out.println("Elapsed time for this process: " + Utils.getReadableTime(stopTime - startTime));
    }
}
