# java-snap

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.3930921.svg)](https://doi.org/10.5281/zenodo.3930921)


This java project focuses on processing RADARSAT-2 fully polarimetric (quad-pol) images. 
Written in the java language, it relies on [snap-engine](https://github.com/senbox-org/snap-engine) and [s1tbx](https://github.com/senbox-org/s1tbx) modules, developped by ESA as part of their [SNAP application](http://step.esa.int/main/toolboxes/snap/).

**Associated publications**: 

S. Dufour-Beauséjour, M Bernier, J. Simon, V. Gilbert, J Tuniq, A. Wendleder and A. Roth. (2020) "Snow-covered sea ice in Salluit, Deception Bay, and Kangiqsujuaq: in situ, RADARSAT-2 and TerraSAR-X observations". Manuscript in preparation.


## Processing steps:
- subset (optional)
- radiometric calibration
- convert to C3 matrix
- speckle filtering
- compute polarimetric parameters (general parameters and HAalpha decomposition)
- orthorectify (optional custom DEM)
- mask (optional)


## Input (all absolute paths)
- path(s) to one or many RADARSAT-2 unzipped image folders
- path to target directory
- path to subset shapefile (optional)
- path to DEM (optional)
- path to mask (optional)


## Output
- Beam-DIMAP image file after each processing step
- RGB JPEG quicklooks after major processing steps
- TIFF image file after orthorectification and masking


## Dependencies
snap-engine, s1tbx, geotools, and their associated artifacts

## Work environment
This was developped using Java 8 and Maven 3. 
Version 6.0.0 is used for snap-engine and s1tbx modules.
Two artifacts could not be resolved (missing from [snap.public.repo](http://nexus.senbox.net/nexus/content/repositories/releases/org/esa/)) and are instead provided as jar files in this project:
- javaml-0.1.7
- ajt-2.11

## Contact information
S. Dufour-Beauséjour (SDB)
INRS-ETE in Québec (QC), Canada
s.dufour.beausejour@gmail.com


## Example
example of command line call (remove line breaks):

java -jar java-snap-1.0.jar
ProcessRS2
/Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/working_dir_RS2/Salluit_RS2_test_path.txt
/Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/working_dir_RS2
/Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/VectorData/ROI/maskSalluit_Polygon.shp
/Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/DEM/mnt2.tif
/Volumes/Crabe/Doctorat/BaieDeception/Donnees/Imagerie/VectorData/special_issue_TSX/Water_HudsonBay_1_MultiPolygon.shp

For optional arguments, pass "None" instead of an absolute path.